Perform first DFT calculation
=============================

Using silicon as an example, we will learn DFT - self-consistent calculation (SCF) with the open source software **Quantum Espresso(QE)** to perform DFT calculations.

## SCF calculation

In this step, using silicon as an example, we will learn how to build up a simple model for calculation. Usually for complex structures, it will take more efforts to build. 


### Before we start, load modules first

Which modules to load may depend on which machine we are using. When using ShanghaiTech HPC, after we log in, please load the following modules since they may be used later ...

    module load apps/python/3.7.1

### Input files for QE calculation 

Different from PWmat which we have used last week, QE will include all input parameters and structure in one file. Here is its structure:

    &control                         # &flat ... / is called namelist in Fortran programing
       calculation = 'scf'              # specify this is a SCF calculation: SCF stands for self-consistent field which yields total energy, force, stress, ... for given structure
       restart_mode='from_scratch',     # if start from scratch use "from_scratch"; if start from a break point use "restart".
       prefix='silicon',                # give a prefix for other output files 
       tstress = .true.                 # if print stress (yes here)
       tprnfor = .true.                 # if print force (yes here)
       pseudo_dir = './'                # where is pseudopotential (psp) file? (here it means in this folder)
       outdir='./'                      # where should the program output its outputfiles? (here it means in this folder)
    /
    &system
       ibrav=  0,                    # we use type 0 in order to specify the lattice vectors
       celldm(1) =1.8897259886d0,    # a constant value multiplied to lattice vectors
       nat=  2,                      # we have two atoms in one cell for computation
       ntyp= 1,                      # we only have one type of atoms (i.e. Si)
       ecutwfc =50.0,                # cutoff energy for planewaves (Ryd unit)
       nspin = 1,                    # consider spin: 1 (no spin), 2 (yes with spin)
       occupations = 'fixed',        # how is the electron occupation determined? (fixed: for gapped system, otherwise use smearing)
    /
    &electrons
       mixing_mode = 'plain'
       mixing_beta = 0.7
       conv_thr =  1.0d-8            # when energy difference between two successive step is smaller than this number, the program thinks SCF is converged
    /
   
    &ions                            # a blank namelist, i.e. every parameter belonging to this namelist is using default value
    
    /
    
    ATOMIC_SPECIES
      Si   28.08  Si.SG15.PBE.UPF    # specify atom species and the psp it uses, the middle number is the mass for this type of atoms
    
    K_POINTS {automatic}             # how to sample k point, here we sample the first BZ with 2x2x2 k point
     2  2  2  0  0  0
    
    CELL_PARAMETERS
      0.00000000     2.71535000     2.71535000    # lattice vectors. Here, the program reads lattice vector will be this 3x3 matrix multiplied celldm(1) 
      2.71535000     0.00000000     2.71535000    #   and the read lattice vector will have Bohr unit!!!
      2.71535000     2.71535000     0.00000000
    
    ATOMIC_POSITIONS (crystal)
      Si     0.00000000     0.00000000     0.00000000   # atomic positions (here use fractional coordinates)
      Si     0.25000000     0.25000000     0.25000000
	

The above parameters are in file in_scf. (You can use other filename you like). **Please remove '#' and comments after #, in Fortran, comment is not provided with #**

### Visualize the structure (**important**)

In the same directory, run "python qei2xsf.py in_scf" (or other filename which is the input file). It will print some coordinates on the screen. Put those coordinates into a file ending with .xsf (e.g. in_relax.xsf); now we can use VESTA to open such .xsf file and check the structure.



### pseudopotential files

For each atomic type, there will be a pseudopotential file. Here, we use Si.SG15.PBE.UPF to represent Si pseudopotential. (Also setup this file in in_scf.)


### Prepare for job launching

Put input file ("in_scf"), pseudopotential file (Si.SG15.PBE.UPF), and job script ("job.pbs" one can use any name for it) into the **same** directory to perform this calculation.
The output file is set in the job script as **out_scf** or any other name you like. If this file is not setup, it will print on the screen. 

    #!/bin/bash
    #PBS  -N   test                   # job name
    #PBS  -l   nodes=1:ppn=16         # resource request: here request 1 node, each node request 16 cores
    #PBS  -l   walltime=4:35:0        # set up a walltime (one should have an estimate of a job, usually a short walltime means fast queue time)
    #PBS  -S   /bin/bash
    #PBS  -j   oe 
    #PBS  -q   spst_pub               # for CPU job, use "spst_pub" queue
    
    cd $PBS_O_WORKDIR
    
    NPROC=`wc -l < $PBS_NODEFILE`
    
    echo This job has allocated $NPROC proc > log
    
    module load apps/quantum-espresso/intelmpi/6.7    # those are the modules used for QE
    module load compiler/intel/2021.3.0
    module load mpi/intelmpi/2021.3.0
    
    mpirun  --bind-to core -np $NPROC -hostfile $PBS_NODEFILE pw.x -npool 4 -ndiag 4 < in_scf >& out_scf             # input file is in_scf, output file is out_scf



### Launch the job

Following the manual of the machine (each machine may have different job script or launching style).

	$ qsub job.pbs                         # submit job

	$ qstat                                # check job status, check manual to see different argument for squeue

	$ qdel jobid                           # kill the job by specifying the job id


### Output files

Most information of output will be in file out_scf. Other output files will include other more detailed info (check the QE manual to see what they are).

Let's see the out_scf. The SCF converges in 11 steps. After the last step, it prints out the occupation for each k point. Below that, the total energy is in the line:

   !    total energy              =     -75.21937799 Ry    



### Practice: converge the cutoff energy and k point sampling

Let's repeat some of the practices we did last week!

**cutoff energy:**

set cutoff energy from 20, 30, 40, 50, 60, and 70. Compare the total energy for each cutoff energies and plot.

**k point sampling:**

From last step of converging cutoff energy, choose the cutoff energy which you believe it is converged. By setting this cutoff energy, let's converge the SCF k point such as 2x2x2, 4x4x4, 6x6x6, and 8x8x8. Compare the total energy for each k point sampling and plot.

After the calculation, let's will find the appropriate cutoff energy and k point sampling.


### Practice: SCF calculation for GaAs

Obtain the structure and coordinates for GaAs primitive and prepare its in_scf (**visualize the structure** VESTA )

Similar to the silicon case above, let's converge **cutoff energy** and **k point sampling**.


## RELAX calculation

For most cases, the structure one can find is not the most optimized structure (i.e. atoms are not in their equilibrium position). It is necessary to let the program to relax the structure and minimize the forces on each atom. 

The way QE (including other softwares) perform relaxation is that after compute total energy and force (given initial structure), it moves the atoms based on their forces, then re-calculate the new total energy and forces. Repeat this process until the **maximum forces** on each atom is below the threshold set in the input files. 

### Silicon exmample

Find out the slicon SCF input file (e.g. in_scf), now let's manually shift one out the two silicon atoms a *little bit* away from its equilibrium position. For example, the first silicon atom coordinates should be (0.000   0.000   0.000), but let's change it to (0.010   0.000   0.000). 

Now the input file for relaxation (file: in_relax) is (**remember to use the converged cutoff energy and k point sampling. Also please remove '#' and comments after #, in Fortran, comment is not provided with #**):

    &control                         
       calculation = 'relax'                                 # this is the change, change scf to relax to perform relaxation       
       restart_mode='from_scratch',     
       prefix='silicon',                
       tstress = .true.                 
       tprnfor = .true.                 
       pseudo_dir = './'                
       forc_conv_thr = 0.001d0,                              # when the maximum forces on all atoms are below 0.001 Ryd/Bohr, the relaxation will terminate
       nstep = 2000,                                         # the maximum of relaxation steps is 2000
       outdir='./'                      
    /
    &system
       ibrav=  0,                    
       celldm(1) =1.8897259886d0,    
       nat=  2,                      
       ntyp= 1,                      
       ecutwfc =50.0,                                     # use the converged total energy
       nspin = 1,                    
       occupations = 'fixed',        
    /
    &electrons
       mixing_mode = 'plain'
       mixing_beta = 0.7
       conv_thr =  1.0d-8            
    /
   
    &ions                            
    
    /
    
    ATOMIC_SPECIES
      Si   28.08  Si.SG15.PBE.UPF    
    
    K_POINTS {automatic}             
     2  2  2  0  0  0                                     # use the converged k point grid
    
    CELL_PARAMETERS
      0.00000000     2.71535000     2.71535000   
      2.71535000     0.00000000     2.71535000   
      2.71535000     2.71535000     0.00000000
    
    ATOMIC_POSITIONS (crystal)
      Si     0.01000000     0.00000000     0.00000000   
      Si     0.25000000     0.25000000     0.25000000

Relaxation calculation will be longer than SCF, since it is repeating SCF calculations. After the relaxation is finished, check the final structure in out_relax (or other filename set up in the job script), what is the coordinates of silicon atoms (including the manually shifted one). If it is still away from 0.0, try using a smaller force convergence threshold (such as 0.0002).


### Check the relaxation structure (**Important**)

In the directory, run the command "python read_qeout_relax.py out_relax". It will generate/print on screen \*.xsf file for visulization in VESTA.


## Plot band structure

Band structure is the eigen states solved for some specific k point path (not a k point regular sampling in a SCF calculation, but a k point path). Since bands calculation is non-SCF, a SCF (or a full relaxation) must be done. And band calculation is performed based on the charge density obtained in SCF simulation. First, make sure the SCF or relaxation calculations are fully converged. 

OK, after the above relaxation, we will plot the band structure of Silicon.

### Obtaining the k point path 

In the same directory where we run relax, have a new input file "in_bands". First, let's decide the k point path (i.e. band structure x axis). We will sample the k point along the following lines (lines starts and ends are called **high symmetry k point, e.g. \Gamma, X, M or R**):

    from \Gamma (0.0, 0.0, 0.0) to X (0.5, 0.0, 0.0) sample 20 k point *evenly*; 
    from X (0.5, 0.0, 0.0) to M (0.5, 0.5, 0.0) sample 20 k point *evenly*; 
    from M (0.5, 0.5, 0.0) to \Gamma (0.0, 0.0, 0.0) sample 20 k point *evenly*; 
    from \Gamma (0.0, 0.0, 0.0) to R (0.5, 0.5, 0.5) sample 20 k point *evenly*; 

total 80 k point. (You need to figure out all the k point coordinates along the above 4 lines.)

### Prepare the input file

The following is a template for bands calculation (file: in_bands): **please remove '#' and comments after #, in Fortran, comment is not provided with #**

    &control                         
       calculation = 'bands'                                 # this is the change, change to bands to perform non-SCF calculation
       restart_mode='from_scratch',     
       prefix='silicon',                
       tstress = .true.                 
       tprnfor = .true.                 
       pseudo_dir = './'                
       outdir='./'                      
    /
    &system
       ibrav=  0,                    
       celldm(1) =1.8897259886d0,    
       nat=  2,                      
       ntyp= 1,                      
       ecutwfc =50.0,                                     # use the same cutoff energy to scf or relax
       nspin = 1,                    
       occupations = 'fixed',        
       nbnd = 8,                                          # Here, one have to specify more bands, otherwise it only prints out valence bands (total band here is 8)
    /
    &electrons
       mixing_mode = 'plain'
       mixing_beta = 0.7
       conv_thr =  1.0d-8            
    /
   
    &ions                            
    
    /
    
    ATOMIC_SPECIES
      Si   28.08  Si.SG15.PBE.UPF    
    
    K_POINTS {crystal}                                    # the generate 80 k point coordinates should be fractional coordinates
      80
    0.0000  0.0000  0.0000    1.0                         # first three numbers are k point coordinate, the last number is the kpoint weight. Since we are doing non-scf, the weight is not import and set to 1 for all k points
    0.0025  0.0000  0.0000    1.0                         # there should be 80 lines for k point coordinates
    0.0050  0.0000  0.0000    1.0
      ...
      ...
    
    CELL_PARAMETERS
      0.00000000     2.71535000     2.71535000   
      2.71535000     0.00000000     2.71535000   
      2.71535000     2.71535000     0.00000000
    
    ATOMIC_POSITIONS (crystal)
      Si     0.00000000     0.00000000     0.00000000     # the bands calculation read structure from silicon.save/date-file-schema.xml (generated by relax or scf calculations). But it is advised to use relaxed structures here. 
      Si     0.25000000     0.25000000     0.25000000


### Launch the job and plot band structures

Please set the output file to be "out_bands", it will print all the eigen values for each k point out of the 80 k points. Use Gamma point as an example, please count how many states are occupied and how many are empty. A simple way for non-spin gapped material is that the number of valence band is num. of electrons divided by 2.

In the same directory, run the command:

    python titledbandstrv5.1.py out_bands num._of_valence_bands  4 4 

num._of_valence_bands: the number of valence bands counted from file out_bands  
title: any name you want to specify  
4: starting energy of y-axis of the band structure is Fermi energy minus 4 eV  
4: ending energy of the y-axis of the band structure is Fermi energy plus 4 eV  

We still find that with 80 k points, parts of the bands are not "that" smooth. In this case, more k points can be sampled between two high symmetry k point: instead of 20, let's try 40 to see if it will work. 

Also, to show a picture on a server, particularly when viewing it from a pc. A X11 server is needed. In pc, download [Xming](https://sourceforge.net/projects/xming/), install and run. In Xshell, for session properties, "tunneling", check X DISPLAY (no need to change the value there). 

See if it will show a graph on the screen. 

By looking at the graph, find out the approximate band gap value. Bad gap is the energy difference between the lowest conduction band minimum (CBM) and highest valence band maximum (VBM). If CBM and VBM are in the same **k** point, we say this material has a direct band gap, otherwise, this is a indirect band gap material. Check out silicon band structure and determined the types of its band gap.


### LDA and GGA computed band gaps

LDA and GGA are two most used exchange-correlation (ex-corr) functionals for DFT calculations. The structural relaxation, cell volume relaxation, and electronic structures could be different depending on which functional is used. In this practice, given the real structure of silicon (Si at 0.0, 0.0, 0.0 and 2nd Si at 0.25, 0.25, 0.25), compute the band structures by using two different functionals: LDA and GGA.

In a different directory, compute the band structures for silicon by using LDA and GGA respectively (run scf first). Find out the band gap and compare.

**Note**: when psp are generated with one functional, it is strongly recommended to use the same functional for DFT calculation. This is also the reason that QE will read the psp and its generating functional to determine the DFT functional automatically. So if one wants to use LDA functional, simply use LDA generated psp. (If you force QE to use different functional which is different from the psp functional, using flag input_dft.)


### Projected density of states (PDOS)

Density of states (DOS) describes the how density the states are for all k points. Another important usage is that it can project the obtained Bloch states onto atomic orbitals (e.g. s, p, d, ...) which illustrates the chemical bonding information for this material. In this practice, we will use GaAs as an example to plot its PDOS.

PDOS is a post-processing of SCF calculation. In a different directory, try SCF of GaAs with its ground state structure. After SCF, prepare the input for a non-SCF calculation. The aim of non-SCF is to obtain the eigen states for more k points. It is quite similar to bands calculation as practiced above, except the bands are computed on a regular k point grid instead of high symmetry lines.

One non-SCF input file:

    &control                         
       calculation = 'nscf'                               # this is the change, change to nscf to perform non-SCF calculation on a regular grid
       restart_mode='from_scratch',     
       prefix='GaAs',                   
       tstress = .true.                 
       tprnfor = .true.                 
       pseudo_dir = './'                
       outdir='./'                      
    /
    &system
       ibrav=  0,                    
       celldm(1) =1.8897259886d0,    
       nat=  2,                      
       ntyp= 2,                      
       ecutwfc =50.0,                                     # use the same cutoff energy to scf
       nspin = 1,                    
       occupations = 'fixed',        
       nbnd = xx,                                         # Here, specify more bands to include conduction band, otherwise it only prints out valence bands (total band here is 8)
    /                                                     #     Let's set this number to be 1.5 * num._of_valence_bands for GaAs case
    &electrons
       mixing_mode = 'plain'
       mixing_beta = 0.7
       conv_thr =  1.0d-8            
    /
   
    &ions                            
    
    /
    
    ATOMIC_SPECIES
      Ga   28.08  Xx.SG15.PBE.UPF    
      As   28.08  Xx.SG15.PBE.UPF    
    
    K_POINTS {automatic}                                    # Sample a denser k point grid compared to SCF
      16 16 16  0  0  0
    
    CELL_PARAMETERS
      x.xxxxxxxx     x.xxxxxxxx     x.xxxxxxxx   
      x.xx  Cell parameters for GaAs  xxxxxxxx   
      x.xxxxxxxx     x.xxxxxxxx     x.xxxxxxxx   
    
    ATOMIC_POSITIONS (crystal)
      x.xxxxxxxx     x.xxxxxxxx     x.xxxxxxxx   
      x.xx  Atomic positions for GaAs  xxxxxxx   
      x.xxxxxxxx     x.xxxxxxxx     x.xxxxxxxx   
      x.xxxxxxxx     x.xxxxxxxx     x.xxxxxxxx   
      ...  ...  ...


Submit this job. After it is finished, it is ready to perform PDOS calculations. Following is the PDOS input file (in_pdos):

    &PROJWFC
       prefix = 'GaAs',
       outdir = './',
       ngauss = 0,
       degauss = 0.005, 
       DeltaE = 0.01, 
    /

PDOS usually uses a small amount of CPU cores compared to its SCF or relax. Put the command in the job script ```mpirun ...(mpirun parameters same to SCF) projwfc.x < in_pdos > out_pdos```

For each atom in the unit cell, projwfc.x will output its projections to s, p, d, ... (all atomic orbitals contained in psp). The practice is by plotting all PDOS in a figure, find out which atom with what orbitals (s, p, or d) are contributing to the CBM and VBM states as the main orbital character.
