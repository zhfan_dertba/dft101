Perform first DFT calculation
=============================

Using silicon as an example, you will learn DFT - self-consistent calculation (SCF) with the GPU-accelerated software **PWmat** to obtain **total energy**.

## Set up your structure

In this step, using silicon as an example, you will learn how to build up a simple model for calculation. Usually for complex structures, it will take more efforts to build. 

### Silicon structure file for PWmat

Different softwares have different flavors for the format of input structure files. For PWmat, here is the example for silicon:

      2                                                         # number of atoms in a cell
      LATTICE                                                   # write "LATTICE" here to indicate that the following three lines are the lattice vector coordinates
           0.00000000     2.71535000     2.71535000             # lattice vector a coordinates in terms of orthogonal lab coordinates (Angstrom)
           2.71535000     0.00000000     2.71535000             # lattice vector b coordinates in terms of orthogonal lab coordinates (Angstrom)
           2.71535000     2.71535000     0.00000000             # lattice vector c coordinates in terms of orthogonal lab coordinates (Angstrom)
      POSITION                                                  # write "POSITION" here to indicate that the following multiple lines are the atomic position coordinates
       14     0.00000000     0.00000000     0.00000000 0 0 0    # First atom, atom type "14" (which is atomic index), "0.00000000     0.00000000     0.00000000" is 
                                                                # the fractional coordinates for this atom in terms of the above lattice vectors, "0 0 0" specify if 
                                                                # this atom will be moved during "structural relaxation" calculation: 0 (fix this atom, without moving 
                                                                # during optimization), 1 (allow this atom to move) 
       14     0.25000000     0.25000000     0.25000000 0 0 0    # Second atom, same format for first one


**Note**: number of atoms should equal to the number of lines for the atomic coordinates block


### Visualize your structure (**important**)

In the same directory, run "./convert_from_config.x <<< atom.config" (you need to copy convert_from_config.x or make a link of this executable to your directory of jobs). You will get xatom.xyz and xatom.xsf. \*.xyz is a simple (x,y,z) coordinates with Angstrom unit (used mostly in molecular systems); \*.xsf is more often used for solid materials. 

Copy xatom.xsf to your local computer and use "VESTA" to open this file and examing the structure. Is the structure reasonable just like you have imagined or visualized in other website?


### Silicon parameter file for PWmat

Next, we specify some import parameter flags to control PWmat calculation (in file etot.input, you have to use this filename):

       4  1                               # specify parallelization style, 4 1 means use 4 GPU to parallize planewaves, and for 
                                          # each of planewave parallization, use 1 GPU to parallize k point (total number of GPU 
                                          # should be 4 * 1 = 4)
       JOB = SCF                          # What the job can be defined
       IN.PSP1 = Si.SG15.PBE.UPF          # specify pseudopotential used here
       IN.ATOM = atom.config              # must to be set, specify filename for structure file
       MP_N123 = 1 1 1 0 0 0              # 1 1 1 means for its first BZ, we only sample 1*1*1 k point (actually only one k point); 
                                          # 0 0 0 means the coordinates of this k point is not shifted
                                          # no seting lead to the default of gamma point  k mesh, here we only try single k point, 
                                          # but we need to converge the number of k point 
       ECUT=50                            # default, for some cases, we also need to converge this parameter
       #rho_error=1e-5                    # commented here, but this flag tells the program once the charge density difference between 
                                          # two SCF steps are smaller than 1e-5, the calculation stops and we think the SCF calculation is converged
       #SCF_ITERO_1 = 6 4 3 0.0 0.025 1   # commented here, each SCF step how to converge

### pseudopotential files

For each atomic type, there will be a pseudopotential file. Here, we use Si.SG15.PBE.UPF to represent Si pseudopotential.


### Prepare for job launching

Put structure file (e.g. atom.config), parameter file (etot.input), pseudopotential file (Si.SG15.PBE.UPF), and job script ("job.slurm" you can use any name for it) into the **same** directory where you have built to perform this calculation.


	#!/bin/sh
	#SBATCH --partition=gpu                # read manual to see different partitions
	#SBATCH --job-name=pwmat               # job name
	#SBATCH --nodes=1                      # for this machine, each node has 4 GPU. Our job uses 4 GPU, so we only need one node
	#SBATCH --ntasks-per-node=4            # our job has 4 copies of parallization (4*1=4, each copy is accelerated by 1 GPU), 
                                               #  thus for this node, we need 4 tasks
	#SBATCH --gres=gpu:4                   # depending on the machine, this machine has 4 GPU per node
	#SBATCH --gpus-per-task=1              # how many GPU we need to accelerate each copy of parallization in our job (we need 1)
	
	module load intel/2016
	module load cuda/8.0
	module load pwmat/2021.11.02           # use model avail pwmat to check the latest pwmat, and module load that version (also specify it here)
	
	mpirun -np 4 -rdma PWmat >& log        # use mpirun to launch this parallel job with 4 tasks (or 4 copies of parallization)


### Launch the job

Following the manual of the machine (each machine may have different job script or launching style).

	$ sbatch job.slurm                     # submit job

	$ squeue -a job.slurm                  # check job status, check manual to see different argument for squeue

	$ scancel jobid                        # kill the job by specifying the job id


### Output files

Most information of output will be in file REPORT. Other output files will include other more detailed info (you can check the PWmat manual to see what they are).

Let's look at one REPORT:

          ## first block: rewrite the full parameters used here (include ones you specified in etot.input)
                    4           1
          PRECISION  = AUTO
          JOB       = SCF
          IN.PSP1   = Si.SG15.PBE.UPF
          IN.ATOM   = atom.config
          CONVERGENCE  = EASY
          ACCURACY  = NORM
          VFF_DETAIL =   1   500  0.50000000E-02     30.00000     4.00000     0.00000     1.00000
          EGG_DETAIL =            3           3           3
          ...
          ...
          ## SCF convergence process 
          ...
          Weighted average num_of_PW for all kpoint=                         1639.000
          ************************************
          E_Hxc(eV)        -50.3233969267177
          E_ion(eV)        -70.7601860983841
          E_Coul(eV)        15.7125351615979
          E_Hxc+E_ion(eV)  -121.083583025102
          NONSCF     1          AVE_STATE_ERROR= 0.1208E+02            ==> did a few steps of non-scf to help convergence
          NONSCF     2          AVE_STATE_ERROR= 0.1738E+01
          ...
          ...
          iter=   7   ave_lin=  4.0  iCGmth=   3                       ==> A SCF step, this step gives total energy -.19781121985421E+03 but with huge energy difference  -.1978E+03
          Ef(eV)               = 0.8375286E+01
          err of ug            = 0.3108E-02
          dv_ave, drho_tot     = 0.0000E+00 0.3089E+00
          E_tot(eV)            = -.19781121985421E+03    -.1978E+03
          -------------------------------------------
          iter=   8   ave_lin=  2.0  iCGmth=   3                       ==> A SCF step, this step gives total energy -.19771440713014E+03 but with much smaller energy difference 0.9681E-01
                                                                       ==>  you can simply find the energy difference between this step and last step total energy to see if this number is correct
          Ef(eV)               = 0.8443520E+01
          err of ug            = 0.4997E-02
          dv_ave, drho_tot     = 0.1931E-01 0.2076E+00
          E_tot(eV)            = -.19771440713014E+03    0.9681E-01
          ...
          ...
          # we say SCF will converge means that two successive SCF steps have a computed total energy difference smaller than we have set in the parameter file. 
          ...
          ...
          ## some summaries of different parts of energies
          ...
          ending_scf_reason = tol Rho_err  5.000000000000000E-005
          Ewald        = -.22853181040038E+03
          Alpha        = -.82603508193298E+01
          E_extV       = 0.00000000000000E+00    0.0000E+00
          E_NSC        = 0.41609077831654E+02    0.2233E-02
          E[-rho*V_Hxc]= 0.43857301631762E+02    -.2179E-02
          E_Hxc        = -.46317887757824E+02    0.1220E-02
          -TS          = -.27832788581607E-17    0.2603E-20
          E_tot(eV)    = -.19764366951412E+03    0.5389E-04
          E_tot(Ryd)   = -.14526541894548E+02    0.1980E-05
          ---------------------------------------------------
          ---------------------------------------------------
          occup for:      kpt=1,spin=1,m=(totN/2-2,totN/2+2)    ==> part of occupation information, more detailed occ info should check "OUT.OCC"
            1.000    1.000    1.000    0.000    0.000
          eigen(eV) for:  kpt=1,spin=1,m=(totN/2-2,totN/2+2)
            8.228    8.228    8.228   10.573   10.573
          ---------------------------------------------------
          ---------------------------------------------------
          ...
          ...



### Practice: converge the k point sampling

1. Copy files in example-Si to the folder you built, following the above steps to launch a SCF calculation to obtain the total energy. Total energy should be a number in output file "REPORT", the last instance of "E_tot(eV)    = -.19764366951412E+03    0.5389E-04". The first big number is the total energy, the second number is SCF convergence energy difference. E_tot(eV) will appear multiple times in REPORT, the last one is the converged one. You can observe how total energy is reduced during SCF convergence. 

2. Compare your obtained REPORT with the REPORT file in output-si to see if their total energyies are the same. How "same" they are?

3. After obtain the total energy for the above calculation (we use 1 1 1 single Gamma point sampling), build a different directory, copy files, but change MP_N123 = 2 2 2 0 0 0 (now, we sample the BZ with 2*2*2 k points without shift). Launch the job again to obtain the total energy

4. Similar to step 2, but we change MP_N123 to 3 3 3 0 0 0 to obtain the energy

5. Similar to step 2, but we change MP_N123 to 4 4 4 0 0 0 to obtain the energy

6. Similar to step 2, but we change MP_N123 to 5 5 5 0 0 0 to obtain the energy

7. Plot the energy as a function of k point sampling size (y axis is total energy; x axis is k point sampling grid size, e.g. 1*1*1, 2*2*2, ...)

### Practice: SCF calculation for GaAs

1. obtain the structure and coordinates for GaAs primitive and prepare its atom.config (**visualize your structure** by using convert_from_config.x and VESTA tools); you could use same etot.input file of Si, but you have to make some changes; find the pseudopotential file and launch the job with MP_N123 = 1 1 1 0 0 0. 

2. Similar to step 1, but we change MP_N123 to 3 3 3 0 0 0 to obtain the energy

3. Similar to step 1, but we change MP_N123 to 4 4 4 0 0 0 to obtain the energy

4. Similar to step 1, but we change MP_N123 to 5 5 5 0 0 0 to obtain the energy

5. Similar to Si, plot the energy as a function of k point sampling size (y axis is total energy; x axis is k point sampling grid size, e.g. 1*1*1, 2*2*2, ...)


