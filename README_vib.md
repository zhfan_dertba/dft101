Use DFT to obtain vibrational frequencies
=============================

With silicon as an example, we will learn how to use DFT with QE to calculation $\Gamma$ point vibrational frequencies and vibrational modes. 

## SCF calculation first

QE needs SCF calculation generated charge density for phonon calculation. Use the previous input file to perform a SCF first.

## Phonon calculation

A great thing of QE is that it uses density functional perturbation theory (DFPT) to obtain phonon frequencies and vibrational modes. It means a phonon at generic q point (phonon wavevector) could be computed using unit cells. Conventional method such as real space dynamic matrix (as illustrated below) has to use supercells for q point other than $\Gamma$ point.

Let's see the input file of $\Gamma$ point phonon calculation in_ph (**Note**, remove '#' and comments after '#' before submitting the job):

```
Silicon primitive cell Gamma phonon
&INPUTPH
  prefix = 'silicon',                  # this should be consistent to SCF calculation
  outdir = './',
  verbosity = 'high',
  recover = .false.,
  amass(1) = 28.085,                   # mass of atom type 1, this is important! specify the correct mass
  tr2_ph = 1e-13,
  fildyn = 'silicon-matdyn',
  trans = .true.,
/
0  0  0                                # coordinates for q point, here only $\Gamma$ point is computed (which is the BZ center)
```

Phonon calculation is much more expensive than SCF and it takes longer. Let's use a bit parallelization (e.g. 4 cores). In the job script, instead of using pw.x, use ```ph.x < in_ph > out_ph```.

Remember the acoustic sum rule (ASR)?!! After the calculation, particularly at $\Gamma$ point, the ASR is needed to shift the acoustic mode frequencies to be zero at $\Gamma$. Setup ASR input as (in_dynmat):

```
&input
  fildyn = 'silicon-matdyn',
  amass(1) = 28.085,
  asr = 'crystal',
/
```

Run ```dynmat.x < in_dynmat > out_dynmat```. Since this is a fast post-processing calculation, use single core is enough. Now see the output out_dynmat and find out the phonon frequencies at $\Gamma$ point. Open dynmat.out to see the vibrational mode for each mode and each atom. Since $\Gamma$ point is a high symmetry point, all the modes are real (the imaginary part is zero), which means they can be plotted!

**:warning: How to plot arrow in VESTA automatically**, simply append the arrow coordinates to each atomic position in *.xsf file and read into VESTA.


## Compute $\Gamma$ point phonon using real space dynamical matrix

Now let's try a different way to compute the phonon frequencies @ $\Gamma$. Remember that phonon freq and mode are obtained by diagonalizing dynamical matrix (DM). For BZ zone center $\Gamma$, we can use single unit cell to build the DM. DM is defined as second derivative of energy with respect to atomic displacement **or** first derivative of force with respect to displacement (of course the mass of atoms are also included). The second definition will be used here. What we will do is to manually shift one atom with a very small displacement (along x, y, and z), and compute the forces on all other atoms (including the shifted one) and compute the derivative to build up the DM.

Here, instead of shifting the atom along lattice vectors, it is needed to shift along lab coordinates (x, y, or z); it is also the coordinates with respect to which the cell_parameters are written. Thus, it is more convenient to write the atomic positions in terms of lab coordinates instead of lattice vectors in the input file. 

```
&control                     
   calculation = 'scf'                # this is a SCF, we only need the force
   restart_mode='from_scratch', 
   prefix='silicon',            
   tstress = .true.             
   tprnfor = .true.             
   pseudo_dir = './'            
   outdir='./'                  
/
&system
   ibrav=  0,                
   celldm(1) =1.8897259886d0,
   nat=  2,                  
   ntyp= 1,                  
   ecutwfc =50.0,            
   nspin = 1,                
   occupations = 'fixed',    
/
&electrons
   mixing_mode = 'plain'
   mixing_beta = 0.7
   conv_thr =  1.0d-8        
/

&ions                        

/

ATOMIC_SPECIES
  Si   28.08  Si.SG15.PBE.UPF

K_POINTS {automatic}         
 2  2  2  0  0  0                      # use converged k point grid

CELL_PARAMETERS
  0.00000000     2.71535000     2.71535000 
  2.71535000     0.00000000     2.71535000 
  2.71535000     2.71535000     0.00000000

ATOMIC_POSITIONS (angstrom)
  Si     0.01000000     0.00000000     0.00000000   
  Si     x.xxxxx  compute these number  xxxxxxxxx
```

The first atom is shited along x for 0.01 Angstrom. After this SCF, compute all the forces F1 (force on atom1) and F2 (force on atom2). 

Once done, shift the first atom along x for -0.01 Angstrom, and compute all the forces F1' and F2'. Now, we can get the first row of DM (DM has the dimension 3*natom x 3*natom). DM[1,1] = (F1'(x) - F1(x))/(2*0.01)/sqrt(m1*m1), DM[1,2] = (F1'(y) - F1(y))/(2*0.01)/sqrt(m1*m1), DM[1,3] = (F1'(z) - F1(z))/(2*0.01)/sqrt(m1*m1), DM[1,4] = (F2'(x) - F2(x))/(2*0.01)/sqrt(m1*m2), DM[1,5] = (F2'(y) - F2(y))/(2*0.01)/sqrt(m1*m2), and DM[1,6] = (F2'(z) - F2(z))/(2*0.01)/sqrt(m1*m2). **Remeber** to use Hartree atomic units (a.u.). 

Do the similar thing for second atom along x, y, and z. Each direction needs + and - displacement. So in total, twelve SCF calculations are needed. 
